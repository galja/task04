package com.epam.model;

import java.util.Random;

public class RandomArrayGenerator {
    private int length;

    public int[] getRandomArray(int min, int max) {
        length = getNextNumber(min, max);
        int[] randomArray = new int[length];
        for (int i = 0; i < length; i++) {
            randomArray[i] = getNextNumber(min, max);
        }
        return randomArray;
    }

    private int getNextNumber(int min, int max) {
        return new Random().nextInt((max - min) + 1) + min;
    }
}
