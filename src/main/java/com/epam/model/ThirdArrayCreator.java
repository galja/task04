package com.epam.model;

public class ThirdArrayCreator extends ArrayCalculator {

    public int[] createUniqueElementsArray(int[] firstArray, int[] secondArray) {
        int[] resultArray = new int[0];
        int[] arrayOfCommonElements = createCommonElementsArray(firstArray, secondArray);

        resultArray = makeArr(firstArray, arrayOfCommonElements, resultArray);
        resultArray = makeArr(secondArray, arrayOfCommonElements, resultArray);
        return resultArray;
    }

    private int[] makeArr(int[] arr, int[] commonEl, int[] resultArray) {
        for (int i = 0; i < arr.length; i++) {
            if (isUnique(arr[i], commonEl)) {
                resultArray = addElement(resultArray, arr[i]);
            }
        }
        return resultArray;
    }

    private boolean isUnique(int el, int[] arr) {
        boolean isUnique = true;
        for (int j = 0; j < arr.length; j++) {
            if (el == arr[j]) {
                isUnique = false;
            }
        }
        return isUnique;
    }

    public int[] createCommonElementsArray(int[] firstArray, int[] secondArray) {
        int[] resultArray = new int[0];
        T:
        for (int i = 0; i < firstArray.length; i++) {
            for (int j = 0; j < secondArray.length; j++) {
                if (firstArray[i] == secondArray[j]) {
                    resultArray = addElement(resultArray, firstArray[i]);
                    continue T;
                }
            }
        }
        return resultArray;
    }
}
