package com.epam.model;

import java.util.Arrays;

public class AllIdenticalElementsRemoval extends ArrayCalculator {
//    private int[] array;
//
//    public void setArray(int[] array) {
//        this.array = array;
//    }
//
//    public int[] getArray() {
//        return array;
//    }

    public int[] removeIdenticalElements(int[] array, int maxIdenticalValuesNumber) {
        int[] resultArray = Arrays.copyOf(array, array.length);
        for (int i = 0; i < array.length; i++) {
            if ((countEntriesNumber(array, array[i]) >= maxIdenticalValuesNumber)) {
                resultArray = deleteElementFromArray(array, array[i]);
            }
        }
        return resultArray;
    }

    private int countEntriesNumber(int[] array, int element) {
        int entriesNumber = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element) {
                entriesNumber++;
            }
        }
        return entriesNumber;
    }
}
