package com.epam.model;

import java.util.Arrays;

public class DuplicateElementsRemoval extends ArrayCalculator {
    public int[] removeDuplicateElements(int[] array) {
        Arrays.sort(array);
        int[] temp = new int[0];
        int newLength;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] != array[i + 1]) {
                temp = addElement(temp, array[i]);
            }
        }
        temp = addElement(temp, array[array.length-1]);
        return temp;
    }
}
