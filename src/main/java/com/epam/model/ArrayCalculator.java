package com.epam.model;

import java.util.Arrays;

public abstract class ArrayCalculator {

    int[] addElement(int[] arr, int el) {
        int[] t = Arrays.copyOf(arr, arr.length);
        arr = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            if (i == arr.length - 1) {
                arr[i] = el;
            } else {
                arr[i] = t[i];
            }
        }
        return arr;
    }

    int[] deleteElementFromArray(int[] arr, int element) {
        int tempLength = 0;
        int[] temp = new int[tempLength];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != element) {
                temp = addElement(temp, arr[i]);
            }
        }
        return temp;
    }
}
