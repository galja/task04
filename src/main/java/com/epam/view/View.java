package com.epam.view;

public interface View {
    void showArray(int[] array);
    int getOperation();
    void showMessage(String message);
}
