package com.epam.view;

import com.epam.helper.Helper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleView implements View {
    private final static Logger logger  = LogManager.getLogger(ConsoleView.class);
    public ConsoleView(){
    }
    @Override
    public void showArray(int[] array) {
       logger.trace(array.toString());
    }
    @Override
    public int getOperation(){
        showMessage(Helper.MENU);
       return scanAnInteger();
    }
    @Override
    public void showMessage(String message){
        logger.trace(message);
    }

    private int scanAnInteger(){
        int number = 0;
        Scanner in = new Scanner(System.in);
        try{
            number = in.nextInt();
        }catch(InputMismatchException e){
            logger.error(Helper.WRONG_INPUT);
            scanAnInteger();
        }
        return number;
    }
}
