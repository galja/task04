package com.epam.helper;

public class Helper {
    public static final String MENU = "To remove all identical elements in array: press 1.\n"+
            "To remove duplicate elements: press 2.\n"+
            "To create third array of elements which are present:\n" +
            "\ta) in both arrays: press 3.\n" +
            "\tb) just in one array: press 4\n";
    public static final String WRONG_INPUT = "Wrong input. Enter an integer from 1 to 4";
    public static final String GENERATED_ARRAY = "Generated array\n";
    public static final int REMOVE_ALL_IDENTICAL = 1;
    public static final int REMOVE_DUPLICATE = 2;
    public static final int RETRIEVE_COMMON_ELEMENTS = 3;
    public static final int RETRIEVE_IDENTICAL_ELEMENTS = 4;
    public static final int MAX_IDENTICAL_VALUES_NUMBER = 2;
    public static final int MAX_VALUE_IN_ARRAY = 10;
    public static final int MIN_VALUE_IN_ARRAY = 0;

}
