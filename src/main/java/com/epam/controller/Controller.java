package com.epam.controller;

import com.epam.helper.Helper;
import com.epam.model.*;
import com.epam.view.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private final static Logger logger = LogManager.getLogger(ConsoleView.class);

    private View view;
    private RandomArrayGenerator arrayGenerator;
    private ArrayCalculator arrayCalculator;

    public Controller() {
        view = new ConsoleView();
        arrayGenerator = new RandomArrayGenerator();
    }

    public void chooseOperation() {
        int operation = view.getOperation();
        int[] array = arrayGenerator.getRandomArray(Helper.MIN_VALUE_IN_ARRAY, Helper.MAX_VALUE_IN_ARRAY);
        view.showMessage(Helper.GENERATED_ARRAY);
        view.showArray(array);
        if (operation == Helper.REMOVE_ALL_IDENTICAL) {
            new AllIdenticalElementsRemoval().removeIdenticalElements(array, Helper.MAX_IDENTICAL_VALUES_NUMBER);
        } else if (operation == Helper.REMOVE_DUPLICATE) {
            new DuplicateElementsRemoval().removeDuplicateElements(array);
        } else if (operation == Helper.RETRIEVE_COMMON_ELEMENTS) {
            arrayCalculator = new ThirdArrayCreator();
        } else if (operation == Helper.RETRIEVE_IDENTICAL_ELEMENTS) {
            arrayCalculator = new ThirdArrayCreator();
        } else {
            logger.error(Helper.WRONG_INPUT);
        }
    }
}
